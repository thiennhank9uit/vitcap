export const types = {
    WORD: "WORD"
}

const initialState = {
    word: ''
}

export default reducer = (state = initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case types.WORD: {
            return {
                ...state,
                word: payload,
            }
        }
    }
}