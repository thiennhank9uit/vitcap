import {AppRegistry} from 'react-native';

import navigatorApp from './config/Navigator.js'

if (typeof process === 'undefined') process = {};
process.nextTick = setImmediate;

module.exports = process;

AppRegistry.registerComponent('vitcap', () => navigatorApp);
