import { StackNavigator } from 'react-navigation';

import Login from '../screens/Login.js';
import Capture from '../screens/Capture.js';
import Detail from '../screens/Detail.js';
import Learned from '../screens/Learned.js';
import Drawer from '../screens/Drawer.js';

export default navigatorApp = StackNavigator({
    Learned: { screen: Learned },
    Login: { screen: Login },
    Capture: { screen: Capture },
    //Learned: { screen: Learned },
    Detail: { screen: Detail },
    Drawer: { screen: Drawer }
});