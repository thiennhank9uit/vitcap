import * as firebase from 'firebase';

let firebaseConfig = {
    apiKey: "AIzaSyCl3GeBnWumf5mQr1LyJwKWJjTTTAPNRYQ",
    authDomain: "vitcap-52450.firebaseapp.com",
    databaseURL: "https://vitcap-52450.firebaseio.com",
    projectId: "vitcap-52450",
    storageBucket: "vitcap-52450.appspot.com",
    messagingSenderId: "807339393181"
};

export default firebaseApp = firebase.initializeApp(firebaseConfig);