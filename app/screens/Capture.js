import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Alert,
    Button,
    ActivityIndicator
} from 'react-native';

import Camera from 'react-native-camera';

const Clarifai = require('clarifai');
const app = new Clarifai.App({
    apiKey: 'accd4d1b0fe44106bed0f986f4047b83'
});

// import { createStore } from 'redux';
// import reducer from '../store/Reducer.js';

// export const store = createStore(reducer)

export default class Capture extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            textImage: 'Hãy chụp đồ vật nào!',
            isPredicting: false
        }
    }

    takePicture() {
        const control = this;
        let textFromImage = '';
        const options = {};
        
        this.camera.capture()
            .then((image64) => {
                app.models
                    .predict(Clarifai.GENERAL_MODEL, { base64: image64.data })
                    .then(
                    function (response) {
                        textFromImage = response.outputs[0].data.concepts[0].name.toString();
                        console.log(textFromImage);
                        if (textFromImage == 'no person')
                            textFromImage = 'Có lỗi, chụp lại nhé!';
                        control.setState({
                            textImage: textFromImage,
                        })
                    },
                    function (err) {
                        console.error(err);
                        control.setState({
                            textFromImage: 'Có lỗi, chụp lại nhé!',
                        })
                    }
                    );
            })
            .catch(err => {
                console.error(err);
                control.setState({
                    textFromImage: 'Có lỗi, chụp lại nhé!',
                })
            });
    }

    renderPredicting() {
        if (this.state.isPredicting)
            return (
                <ActivityIndicator
                    color='green'
                    size='large' />
            )
        else
            return null;
    }

    render() {
        console.disableYellowBox = true;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    captureTarget={Camera.constants.CaptureTarget.memory}>
                    {this.renderPredicting()}
                    <Text style={styles.textImage}> {this.state.textImage} </Text>
                    <Text style={styles.learn} 
                    onPress={() =>  {
                        //store.dispatch({ type: 'WORD', payload: this.state.textImage });
                        navigate('Detail')}}>HỌC</Text>
                    <Text style={styles.capture} onPress={this.takePicture.bind(this)}>[CHỤP]</Text>
                </Camera>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },

    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    },

    learn: {
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 10,
        marginTop: 60,
    },

    textImage: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    }
})