import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Button,
    TextInput,
    StyleSheet,
    ActivityIndicator
} from 'react-native';

import firebaseApp from '../config/Firebase.js';

export default class Login extends Component {
    static navigationOptions = {
        header: null
    };
    
    constructor(props) {
        super(props);
        this.state = {
            scene: 0,
            user: '',
            pass: ''
        }
    }

    componentWillMount() {
        if (this.state.scene == 0)
            this.autherize();
    }

    autherize() {
        if (this.state.scene == 0) {
            try {
                firebaseApp.auth().onAuthStateChanged((user) => {
                    if (this.state.scene == 0)
                        if (user != null) {
                            this.setState({
                                scene: 2
                            });
                        }
                        else {
                            this.setState({
                                scene: 1
                            })
                        }
                });
            }
            catch (error) {
                console.log(error.toString())
            }
        }

    }

    login() {
        firebaseApp.auth().signInWithEmailAndPassword(this.state.user, this.state.pass).then((userData) => {
            this.setState({
                scene: 2
            })
        }
        ).catch((error) => {
            this.setState({
                scene: 1
            })
            console.log(error.toString())
        });
    }

    register() {
        firebaseApp.auth().createUserWithEmailAndPassword(
            this.state.user,
            this.state.pass).then(() => {
                this.setState({
                    scene: 2
                })
            }).catch((error) => {
                this.setState({
                    scene: 1
                })

            });
    }

    renderAuthorizing() {
        return (
            <View style={styles.containLoading}>
                <ActivityIndicator
                    color='green'
                    size='large' />
                <Text style={styles.textAuthorize}>
                    Đang xác nhận thông tin user!
                </Text>
            </View>
        )
    }

    renderLogin() {
        const { navigate } = this.props.navigation;

        return (
            <View style={styles.container}>
                <Text style={styles.title}>
                    THÔNG TIN ĐĂNG NHẬP
                </Text>
                <TextInput
                    style={styles.input}
                    placeholderTextColor='chocolate'
                    placeholder="Email"
                    onChangeText={(user) => { this.state.user = user }}

                />
                <TextInput
                    style={styles.input}
                    placeholderTextColor='chocolate'
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={(pass) => { this.state.pass = pass }}
                />
                <View style={styles.rowButton}>
                    <TouchableOpacity style={styles.sizeButton}>
                        <Button
                            color='chocolate'
                            title='Login'
                            onPress={() => {
                                this.login()
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.sizeButton}>
                        <Button
                            color='chocolate'
                            title='Register'
                            onPress={() => {
                                this.register();
                            }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        console.disableYellowBox = true;

        const { navigate } = this.props.navigation;

        //Authorizing
        if (this.state.scene == 0)
            return (
                this.renderAuthorizing()
            )

        //Login
        if (this.state.scene == 1)
            return (
                this.renderLogin()
            )

        //Navigate next scene
        if (this.state.scene == 2) {
            navigate('Capture');
            this.setState({
                scene: 1,
            });
            return null;
        }
    }
}

const styles = StyleSheet.create({
    textAuthorize: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },

    containLoading: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'chocolate'
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

    rowButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: 300
    },

    title: {
        fontSize: 20,
        color: 'chocolate',
        textAlign: 'center'
    },

    input: {
        width: 300,
        borderBottomColor: 'blue',
    },

    sizeButton: {
        width: 120,
    },
})