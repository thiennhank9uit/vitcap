import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';
import Tts from 'react-native-tts';

export default class Detail extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        console.disableYellowBox = true;
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.word}> Từ Tiếng Anh </Text>
                <View style={styles.imageContainer}>
                    <Image
                        source={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }}
                        style={{ width: 300, height: 300 }}
                    />
                </View>
                <Text style={styles.word}> Từ Tiếng Việt </Text>
                <View style={styles.botContainer}>
                    <View style={styles.backContainer}>
                        <TouchableOpacity 
                        style={{ width: 50, height: 50 }}
                        onPress={() => 
                            navigate('Capture')
                            }>
                        <Image
                            source={require('../res/images/back.png')}
                            style={{ width: 50, height: 50 }}
                        />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.speakerContainer}
                        onPress={() => console.log('pressed')}>
                        <Image
                            source={require('../res/images/speaker.png')}
                            style={{ width: 50, height: 50 }}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },

    word: {
        marginTop: 40,
        fontSize: 30,
        color: 'chocolate',
        fontWeight: 'bold',
    },

    imageContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },

    botContainer: {
        width: 300,
        marginTop: 40,
        flexDirection: 'row',
    },

    speakerContainer: {
        flex: 1 / 4,
        marginLeft: 200,
    },

    backContainer: {
        flex: 1 / 4,
    }
})